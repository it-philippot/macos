#!/bin/bash


function check_trim()
{
 TRIM_OK=$(system_profiler SPSerialATADataType | grep 'TRIM' | grep -c 'Yes')
 if [ "z$TRIM_OK" == "z1" ]
 then
  echo "TRIM is enabled" 
 else
  echo "About to enable TRIM"
  sudo trimforce enable 
 fi
 
}

function install_homebrew ()
{
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 
}

function install_brews()
{
brew install git
brew install cask
}

function create_directories()
{
pushd $HOME
mkdir -p Platinum Silver WorkingCopies
mkdir -p Applications/bin
popd
}

function clone_macos()
{
pushd $HOME
mkdir -p WorkingCopies/it-philippot
cd  WorkingCopies/it-philippot
git clone https://gitlab.com/it-philippot/macos.git
cd macos
git config user.email "etienne@charlier.info"
popd
}

function change_macos_remote()
{
pushd $HOME
mkdir -p WorkingCopies/it-philippot/macos
git remote set-url origin git@ech1965.gitlab.com:it-philippot/macos.git 
popd
}


check_trim
install_homebrew
install_brews
create_directories
clone_macos


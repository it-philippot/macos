#!/bin/bash
pushd $HOME/Applications

DIR=syncthing-macosx-amd64-v0.14.8
FILE=$DIR.tar.gz
curl -sL https://github.com/syncthing/syncthing/releases/download/v0.14.8/$FILE >$FILE
tar xvzf $FILE
mv $DIR syncthing
popd


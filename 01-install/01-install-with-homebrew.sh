#!/bin/bash

# Voici les appli qui n'ont pas de cask
# duplicati

# Install with brew
brew install rbenv
brew install dvm
brew install ruby-build
brew install exiftool
brew install sbt
brew install golang
brew install glide
brew install bash-completion
brew install git-flow
brew install mono
brew install iperf
brew install maven
brew install wget
brew install python3
# Mac App Store access
brew install mas 
# surveillance de répertories
brew install fswatch


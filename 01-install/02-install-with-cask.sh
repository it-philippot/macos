#!/bin/bash

# Installation des cask
brew update
brew upgrade 
brew cleanup

# Install applications using cask
brew cask install thunderbird
brew cask install firefox
brew cask install google-chrome
brew cask install java
brew cask install virtualbox
brew cask install virtualbox-extension-pack
brew cask install postman
brew cask install libreoffice
brew cask install calibre
brew cask install docker
brew cask install tunnelblick
brew cask install macpass
brew cask install box-sync
brew cask install dropbox
brew cask install itsycal
#brew cask install xtrafinder
brew cask install chefdk
brew cask install sourcetree
brew cask install jetbrains-toolbox
brew cask install qsyncthingtray
#brew cask install 

